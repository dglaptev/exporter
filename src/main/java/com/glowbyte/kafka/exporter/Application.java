package com.glowbyte.kafka.exporter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.FileInputStream;
import java.util.Properties;

public class Application {
    private static Logger log = LoggerFactory.getLogger(Application.class);

    public static void main(String[] args) {
        log.info("Application starting...");

        Properties properties = new Properties();
        try {
            String configurationFile = System.getProperty("conf");
            if (configurationFile == null) {
                throw new Exception("No configuration file found (check -Dconf argument)");
            }
            FileInputStream fileInputStream = new FileInputStream(configurationFile);
            properties.load(fileInputStream);
        } catch (Exception e) {
            log.error("Error", e);
            System.exit(1);
        }

        Exporter exporter = new Exporter(properties);
        exporter.doStart();

        log.info("Application started.");
    }

}
