package com.glowbyte.kafka.exporter;

import com.codahale.metrics.MetricAttribute;
import com.codahale.metrics.MetricFilter;
import com.codahale.metrics.MetricRegistry;
import com.codahale.metrics.graphite.Graphite;
import com.codahale.metrics.graphite.GraphiteReporter;
import org.apache.kafka.clients.admin.*;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.clients.consumer.OffsetAndMetadata;
import org.apache.kafka.common.TopicPartition;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.UnknownHostException;
import java.util.*;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public class Exporter extends Thread {
    private final Logger log = LoggerFactory.getLogger(getClass());
    private final int metricsIntervalMs;
    private final Pattern topicWhiteListPattern;
    private final Pattern topicBlackListPattern;
    private final Pattern groupWhiteListPattern;
    private final Pattern groupBlackListPattern;
    private String prefix;
    private volatile boolean running = false;
    private AdminClient adminClient;
    private KafkaConsumer consumer;
    private boolean reportByTopic;
    private boolean reportByPartition;
    private MetricRegistry metricRegistry;
    private Properties properties;
    private int warnDurationMs;

    Exporter(Properties properties) {
        setName("Exporter");

        properties.putIfAbsent("key.deserializer", "org.apache.kafka.common.serialization.StringDeserializer");
        properties.putIfAbsent("value.deserializer", "org.apache.kafka.common.serialization.StringDeserializer");

        String bootstrapServers = properties.getProperty("bootstrap.servers");
        String cluster = bootstrapServers
                .replace('.', '_')
                .replace(',', '_')
                .replace('-', '_')
                .replace(':', '_');

        String hostPrefix;
        try {
            hostPrefix = InetAddress.getLocalHost().getHostName().replace(".", "-");
        } catch (UnknownHostException e) {
            hostPrefix = "unknown-host";
        }
        prefix = hostPrefix + ".exporter.kafka." + cluster;

        metricsIntervalMs = Integer.parseInt(properties.getProperty("metrics.interval.ms", "15000"));

        if (metricsIntervalMs < 5000) {
            log.error("Parameter metrics.interval.ms must be greater than 5000");
            System.exit(1);
        }

        warnDurationMs = Integer.parseInt(properties.getProperty("warn.duration.ms", "50"));
        if (warnDurationMs < 0) {
            log.error("Parameter warn.duration.ms must be greater than 0");
            System.exit(1);
        }

        reportByPartition = Boolean.parseBoolean(properties.getProperty("report.per.partition", "false"));
        reportByTopic = Boolean.parseBoolean(properties.getProperty("report.per.topic", "true"));


        adminClient = AdminClient.create(properties);
        consumer = new KafkaConsumer(properties);

        //graphite
        metricRegistry = new MetricRegistry();
        String graphtiteServer = properties.getProperty("graphite.host", "localhost");
        int graphtitePort = Integer.parseInt(properties.getProperty("graphite.port", "2003"));
        int graphtiteIntervalMs = Integer.parseInt(properties.getProperty("graphite.interval.ms", "15000"));
        Graphite graphite = new Graphite(new InetSocketAddress(graphtiteServer, graphtitePort));
        Set<MetricAttribute> disabled = new HashSet<>();
        disabled.add(MetricAttribute.MIN);
        disabled.add(MetricAttribute.STDDEV);
        disabled.add(MetricAttribute.P50);
        disabled.add(MetricAttribute.P75);
        disabled.add(MetricAttribute.P95);
        disabled.add(MetricAttribute.P98);
        disabled.add(MetricAttribute.P99);
        disabled.add(MetricAttribute.P999);
        disabled.add(MetricAttribute.COUNT);
        disabled.add(MetricAttribute.M1_RATE);
        disabled.add(MetricAttribute.M5_RATE);
        disabled.add(MetricAttribute.M15_RATE);
        disabled.add(MetricAttribute.MEAN_RATE);
        GraphiteReporter graphiteReporter = GraphiteReporter.forRegistry(metricRegistry)
                .convertRatesTo(TimeUnit.SECONDS)
                .convertDurationsTo(TimeUnit.MILLISECONDS)
                .filter(MetricFilter.ALL)
                .disabledMetricAttributes(disabled)
                .build(graphite);
        graphiteReporter.start(graphtiteIntervalMs, TimeUnit.MILLISECONDS);

        String topicWhiteListRegex = properties.getProperty("topic.whitelist.regex", ".*");
        String topicBlackListRegex = properties.getProperty("topic.blacklist.regex", "");
        String groupWhiteListRegex = properties.getProperty("group.whitelist.regex", ".*");
        String groupBlackListRegex = properties.getProperty("group.blacklist.regex", "");

        topicWhiteListPattern = Pattern.compile(topicWhiteListRegex);
        topicBlackListPattern = Pattern.compile(topicBlackListRegex);
        groupWhiteListPattern = Pattern.compile(groupWhiteListRegex);
        groupBlackListPattern = Pattern.compile(groupBlackListRegex);

        this.properties = properties;
    }

    void doStart() {
        log.trace("Starting...");
        running = true;
        start();
        log.trace("Started.");
    }

    @Override
    public void run() {
        while (running) {
            try {
                getMetrics();
                sleep(metricsIntervalMs);
            } catch (Exception e) {
                log.error("Error.", e);
                adminClient = AdminClient.create(properties);
                consumer = new KafkaConsumer(properties);
            }
        }
    }

    private void getMetrics() throws ExecutionException, InterruptedException, TimeoutException {
        log.trace("Getting metrics...");
        ListTopicsOptions listTopicsOptions = new ListTopicsOptions();
        listTopicsOptions.listInternal(false);
        Set<String> topics = adminClient.listTopics(listTopicsOptions).names().get(1000, TimeUnit.MILLISECONDS);

        for (String topic: topics) {
            if (!topicWhiteListPattern.matcher(topic).matches() || topicBlackListPattern.matcher(topic).matches()) continue;
            //log.trace("Topic: {}", topic);
            getTopicMetadata(topic);
        }
        log.trace("Done.");
    }

    private void getTopicMetadata(String topicName) throws ExecutionException, InterruptedException {
        long startTime = System.nanoTime();
        //https://stackoverflow.com/questions/55937806/apache-kafka-get-list-of-consumers-on-a-specific-topic

        List<String> groupIds = adminClient.listConsumerGroups().all().get().stream()
                .map(ConsumerGroupListing::groupId)
                .filter(s -> groupWhiteListPattern.matcher(s).matches() && !groupBlackListPattern.matcher(s).matches())
                .collect(Collectors.toList());

        Map<String, ConsumerGroupDescription> groups = adminClient.describeConsumerGroups(groupIds).all().get();

        for (String groupId : groupIds) {
            //if (!groupWhiteListPattern.matcher(groupId).matches() || groupBlackListPattern.matcher(groupId).matches()) continue;
            getGroupMetadata(groupId, groups, topicName);
        }
        long endTime = System.nanoTime();
        long duration = endTime - startTime;
        if (duration/1000000 > warnDurationMs) log.warn("Topic {} - {} ms, {} groups checked.", topicName, duration/1000000, groupIds.size());

    }

    private void getGroupMetadata(String groupId, Map<String, ConsumerGroupDescription> groups, String topicName) throws ExecutionException, InterruptedException {
        //log.trace("Group: {}", groupId);
        long startTime = System.nanoTime();

        List<TopicPartition> topicPartitions = new ArrayList<>();

        //ConsumerGroupDescription descr = groups.get(groupId);

        groups.get(groupId).members().stream()
                .map(s -> s.assignment().topicPartitions())
                .flatMap(Collection::stream)
                .filter(s -> s.topic().equals(topicName))
                .forEach(topicPartitions::add);

        //if (log.isTraceEnabled()) log.trace("Partitions: {}", topicPartitions.toString());

        if (topicPartitions.isEmpty()) return;

        ListConsumerGroupOffsetsResult offsetsResult = adminClient.listConsumerGroupOffsets(groupId);
        Map<TopicPartition, OffsetAndMetadata> offsets = offsetsResult.partitionsToOffsetAndMetadata().get();
        Map latestOffsets = consumer.endOffsets(topicPartitions);

        long sumLatestOffset = 0;
        long sumCurrentOffset = 0;
        long sumLag = 0;
        for (TopicPartition topicPartition: topicPartitions) {
            int partition = topicPartition.partition();
            long currentOffset;
            long lag;
            long latestOffset;
            if (offsets.containsKey(topicPartition)) {
                currentOffset = offsets.get(topicPartition).offset();
            } else {
                log.warn("Current offsets {} does not contains {}", offsets, topicPartition);
                continue;
            }
            if (latestOffsets.containsKey(topicPartition)) {
                latestOffset = (long) latestOffsets.get(topicPartition);
                lag = latestOffset - currentOffset;
            } else {
                log.warn("Offsets {} does not contains {}", latestOffsets, topicPartition);
                continue;
            }

            sumCurrentOffset += currentOffset;
            sumLatestOffset += latestOffset;
            sumLag += lag;

            if (reportByPartition) {
                if (log.isTraceEnabled()) {
                    log.trace(prefix + ".offset.latest." + topicName + "." + partition + " " + latestOffset);
                    log.trace(prefix + ".offset.current." + topicName + "." + groupId + "." + partition + " " + currentOffset);
                    log.trace(prefix + ".offset.lag." + topicName + "." + groupId + "." + partition + " " + lag);
                }

                metricRegistry.histogram(prefix + ".offset.latest." + topicName + "." + partition).update(latestOffset);
                metricRegistry.histogram(prefix + ".offset.current." + topicName + "." + groupId + "." + partition).update(currentOffset);
                metricRegistry.histogram(prefix + ".offset.lag." + topicName + "." + groupId + "." + partition).update(lag);
            }
        }

        if (reportByTopic) {
            if (log.isTraceEnabled()) {
                log.trace(prefix + ".offset.latest." + topicName + " " + sumLatestOffset);
                log.trace(prefix + ".offset.current." + topicName + "." + groupId + " " + sumCurrentOffset);
                log.trace(prefix + ".offset.lag." + topicName + "." + groupId + " " + sumLag);
            }
            metricRegistry.histogram(prefix + ".offset.latest." + topicName).update(sumLatestOffset);
            metricRegistry.histogram(prefix + ".offset.current." + topicName + "." + groupId).update(sumCurrentOffset);
            metricRegistry.histogram(prefix + ".offset.lag." + topicName + "." + groupId).update(sumLag);

        }
        long endTime = System.nanoTime();
        long duration = endTime - startTime;
        if (duration/1000000 > warnDurationMs) log.warn("Topic {}, group {} - {} ms.", topicName, groupId, duration/1000000);
    }
}
